package scripts.wastedbro.api.kBehaviorTree.nodes

import org.tribot.api.General
import scripts.wastedbro.api.kBehaviorTree.BehaviorTreeStatus
import scripts.wastedbro.api.kBehaviorTree.Composite
import scripts.wastedbro.api.kBehaviorTree.IBehaviorNode
import scripts.wastedbro.api.kBehaviorTree.IParentNode
import java.util.ArrayList

/**
 * Composite Node
 * A sequence node will run its children until 1 fails.
 * If a child fails, the sequence will report failure.
 * If all the nodes run and succeed, the sequence will report success.
 */
class SequenceNode(private var _name: String = "") : Composite()
{
    override var name: String
        get() = "[Sequence] $_name"
        set(n) { this._name = n }

    override fun tick(): BehaviorTreeStatus
    {
        for (node in children)
        {
            var status: BehaviorTreeStatus
            do
            {
                status = node.tick()

                if (status === BehaviorTreeStatus.RUNNING)
                    General.sleep(50, 250)
            }
            while (status === BehaviorTreeStatus.RUNNING)

            if (status !== BehaviorTreeStatus.SUCCESS)
                return status
        }

        return BehaviorTreeStatus.SUCCESS
    }
}
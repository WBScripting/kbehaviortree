package scripts.wastedbro.api.kBehaviorTree.nodes

import scripts.wastedbro.api.kBehaviorTree.BehaviorTreeStatus
import scripts.wastedbro.api.kBehaviorTree.Decorator

class InverterNode(private var _name: String = "") : Decorator()
{
    override var name: String
    get() = "[Inverter] $_name"
    set(n) { this._name = n }

    override fun tick(): BehaviorTreeStatus
    {
        if (child == null)
            throw RuntimeException("Inverter node ($name) has no children")

        val status = child!!.tick()

        return when {
            status === BehaviorTreeStatus.SUCCESS -> BehaviorTreeStatus.FAILURE
            status === BehaviorTreeStatus.FAILURE -> BehaviorTreeStatus.SUCCESS
            else -> status
        }
    }
}
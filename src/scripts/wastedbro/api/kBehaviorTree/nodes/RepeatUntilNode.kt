package scripts.wastedbro.api.kBehaviorTree.nodes

import org.tribot.api.General
import scripts.wastedbro.api.kBehaviorTree.BehaviorTreeStatus
import scripts.wastedbro.api.kBehaviorTree.Decorator

class RepeatUntilNode() : Decorator()
{
    private var _name: String = ""
    override var name: String
        get() = "[RepeatUntil] $_name"
        set(n) { this._name = n }

    private var status: BehaviorTreeStatus? = null
    private var stopCondition: (() -> Boolean)? = null

    constructor(name: String = "", stopCondition: () -> Boolean, status: BehaviorTreeStatus) : this()
    {
        this.name = name
        this.stopCondition = stopCondition
        this.status = status
    }

    constructor(name: String = "", stopCondition: () -> Boolean) : this()
    {
        this.name = name
        this.stopCondition = stopCondition
    }

    constructor(name: String = "", status: BehaviorTreeStatus) : this()
    {
        this.name = name
        this.status = status
    }

    override fun tick(): BehaviorTreeStatus
    {
        if (child == null)
            throw RuntimeException("RepeatUntil node ($name) has no children")

        var childStatus = child!!.tick()

        if (stopCondition == null)
        {
            while (childStatus !== this.status)
            {
                childStatus = child!!.tick()
                if (childStatus == BehaviorTreeStatus.KILL)
                    break
                General.sleep(50, 350)
            }
        }
        else
        {
            while (childStatus !== this.status && !stopCondition!!())
            {
                childStatus = child!!.tick()
                if (childStatus == BehaviorTreeStatus.KILL)
                    break
                General.sleep(50, 350)
            }
        }

        return childStatus
    }
}
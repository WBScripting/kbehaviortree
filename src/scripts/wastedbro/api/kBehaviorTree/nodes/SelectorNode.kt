package scripts.wastedbro.api.kBehaviorTree.nodes

import org.tribot.api.General
import scripts.wastedbro.api.kBehaviorTree.BehaviorTreeStatus
import scripts.wastedbro.api.kBehaviorTree.Composite
import scripts.wastedbro.api.kBehaviorTree.IBehaviorNode
import scripts.wastedbro.api.kBehaviorTree.IParentNode
import java.util.ArrayList

/**
 * Composite Node
 * A selector node will run its children until 1 succeeds.
 * If a child succeeds, the selector will report success.
 * If all the children run and fail, the selector will report failure.
 */
class SelectorNode(private var _name: String = "") : Composite()
{
    override var name: String
        get() = "[Selector] $_name"
        set(n) { this._name = n }

    override fun tick(): BehaviorTreeStatus
    {
        for (node in children)
        {
            var status: BehaviorTreeStatus
            do
            {
                status = node.tick()
                if (status === BehaviorTreeStatus.RUNNING)
                    General.sleep(50, 250)
            }
            while (status === BehaviorTreeStatus.RUNNING)

            if (status !== BehaviorTreeStatus.FAILURE)
                return status
        }

        return BehaviorTreeStatus.FAILURE
    }
}
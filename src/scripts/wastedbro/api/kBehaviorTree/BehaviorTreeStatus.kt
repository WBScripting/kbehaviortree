package scripts.wastedbro.api.kBehaviorTree

enum class BehaviorTreeStatus
{
    SUCCESS,
    FAILURE,
    RUNNING,
    KILL
}
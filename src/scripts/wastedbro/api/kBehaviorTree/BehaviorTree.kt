package scripts.wastedbro.api.kBehaviorTree

class BehaviorTree<T>(var dataContext: T? = null) : Decorator()
{
    override var name: String = "Root"

    override fun tick(): BehaviorTreeStatus
    {
        return this.child?.tick() ?: BehaviorTreeStatus.FAILURE
    }

    override fun getTreeString(prefix: String): String
    {
        return name + "\n" + child?.getTreeString("    ")
    }
}